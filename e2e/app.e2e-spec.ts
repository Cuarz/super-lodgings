import { SuperLodgingsPage } from './app.po';

describe('super-lodgings App', () => {
  let page: SuperLodgingsPage;

  beforeEach(() => {
    page = new SuperLodgingsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
