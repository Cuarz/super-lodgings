var detailButtons = '';
var detailHotels = '';
var dates = '';
var roomTypes = '';

init();

function init() {
	// node with the available hotels
    var target = document.querySelector('#js_center_content');

    // create an observer instance to check for DOM changes in the main content and update our data
    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            loadData();
        });
    });

    // configuration of the observer:
    var config = { characterData: true, attributes: false, childList: false, subtree: true };

    observer.observe(target, config);
}

function loadData() {
	detailButtons = document.getElementsByClassName('icon-bg-icn_arrow_deal');
	detailHotels = document.getElementsByClassName('name__copytext m-0 item__slideout-toggle');
	dates = document.getElementsByClassName('btn-horus__value js-btn-calendar-value');
	roomTypes = document.getElementsByClassName('btn-horus__value js-btn-roomtype-value');
	addClickEvents();
}

function createInfoObject(hotel, startDate, endDate, roomType) {
	return {
		hotel: hotel.innerHTML,
		startDate: startDate.innerHTML,
		endDate: endDate.innerHTML,
		roomType: roomType.innerHTML
	}
}

function addClickEvents() {
	for(var i = 0; i < detailButtons.length; i+=1) {
		(function (index) {
			var infoToSend = createInfoObject(detailHotels[index], dates[0], dates[1], roomTypes[0]);

			// hotel list does change
			// cloning button to remove any eventListeners, so they don't duplicate
			var buttonClone = detailButtons.item(index).cloneNode(true);
			detailButtons.item(index).parentNode.replaceChild(buttonClone, detailButtons.item(index));

			detailButtons.item(index).addEventListener('click', function() {
				chrome.runtime.sendMessage({ value: 'open', info: infoToSend }, function(response) {
					console.log(response.text);
				});
			}, false);
		}(i));
	}
}
