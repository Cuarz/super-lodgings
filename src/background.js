var URL = chrome.extension.getURL('index.html');
var WINDOWTYPE = 'popup';
var WIDTH = 1080;
var HEIGHT = 720;

init();

function init() {
    messageWatcher();
}

function messageWatcher() {
    chrome.runtime.onMessage.addListener(
        function(request, sender, sendResponse) {
            if (request.value === 'open') {
                sendResponse({text: 'opening super-lodgings'});
                setLocalSearchInfo(request.info);
                openApp();
            }
        }
    );
}

function setLocalSearchInfo(info) {
    localStorage.setItem('searchInfo', JSON.stringify(info));
}

function openApp() {
    chrome.windows.create({ url: URL, type: WINDOWTYPE, width: WIDTH, height: HEIGHT });
}
