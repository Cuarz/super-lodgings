import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SearchComponent } from './search/search.component';
import { RoomListComponent } from './room/room-list/room-list.component';
import { DetailComponent } from './detail/detail.component';
import { BookingComponent } from './booking/booking.component';
import { DoneComponent } from './done/done.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const appRoutes: Routes = [
    { path: 'search', component: SearchComponent },
    { path: 'room-list', component: RoomListComponent },
    { path: 'detail', component: DetailComponent },
    { path: 'booking', component: BookingComponent },
    { path: 'done', component: DoneComponent },
    { path: '',   redirectTo: '/search', pathMatch: 'full' },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
