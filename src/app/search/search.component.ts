import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IMyOptions, IMyDate, IMyDateModel } from 'mydatepicker';

import { Search } from './search';
import { RoomType } from './room-type';
import { SearchService } from './search.service';

declare var moment: any;

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
    search: Search = new Search();
    roomTypes: RoomType[] = [];
    today: Date;
    followingDay: Date;
    nextYearStart: Date;
    nextYearEnd: Date;
    readonly datePickerOptions: IMyOptions = {
        dateFormat: 'dd/mm/yyyy',
        showTodayBtn: false,
        firstDayOfWeek: 'mo',
        sunHighlight: false,
        showClearDateBtn: false,
        inline: true
    };
    arrivalDatePickerOptions: IMyOptions = {};
    departureDatePickerOptions: IMyOptions = {};
    startDate: IMyDate;
    endDate: IMyDate;

    constructor(
        private searchService: SearchService,
        private router: Router) {
    }

    ngOnInit(): void {
        if (!this.searchService.getSearchInfo().startDate || !this.searchService.getSearchInfo().endDate) {
            this.search = this.searchService.getSearchFromLS();
        } else {
            this.search = this.searchService.getSearchInfo();
        }
        this.setDates();
        this.getRoomTypes();
    }

    setDates(): void {
        this.today = moment().subtract(1, 'days').toDate();
        this.followingDay = moment(this.search.startDate).add(1, 'days').toDate();
        this.nextYearStart = moment(this.today).add(1, 'year').toDate();
        this.nextYearEnd = moment(this.followingDay).add(1, 'year').toDate();

        this.setDatePickerOptions();
    }

    setDatePickerOptions(): void {
        const arrivalOptions = this.getCopyOfOptions();
        const departureOptions = this.getCopyOfOptions();

        this.startDate = this.toIMyDate(this.search.startDate);
        this.endDate = this.toIMyDate(this.search.endDate);

        arrivalOptions.disableUntil = this.toIMyDate(this.today);
        arrivalOptions.disableSince = this.toIMyDate(this.nextYearStart);

        departureOptions.disableUntil = this.toIMyDate(this.search.startDate);
        departureOptions.disableSince = this.toIMyDate(this.nextYearEnd);

        this.arrivalDatePickerOptions = arrivalOptions;
        this.departureDatePickerOptions = departureOptions;
    }

    toIMyDate(date: Date): IMyDate {
        return {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()
        };
    }

    onArrivalChange(event: IMyDateModel): void {
        this.search.startDate = event.jsdate;
        this.updateDeparture();
    }

    onDepartureChange(event: IMyDateModel): void {
        this.search.endDate = event.jsdate;
    }

    onSearch(): void {
        this.searchService.setSearchInfo(this.search);
        this.router.navigate(['/room-list']);
    }

    updateDeparture(): void {
        const departureOptionsCopy = this.getCopyOfOptions();
        this.followingDay = this.search.startDate;
        departureOptionsCopy.disableUntil = this.toIMyDate(this.followingDay);
        if (this.search.endDate <= this.search.startDate) {
            this.search.endDate = moment(this.followingDay).add(1, 'days').toDate();
            this.endDate = this.toIMyDate(this.search.endDate);
        }
        this.departureDatePickerOptions = departureOptionsCopy;
    }

    getCopyOfOptions(): IMyOptions {
        return JSON.parse(JSON.stringify(this.datePickerOptions));
    }

    getRoomTypes(): void {
        this.searchService.getRoomTypes().then(
            roomtypes => this.roomTypes = roomtypes
        );
    }

    setRoomType(roomType): void {
        this.search.roomType = roomType;
    }

}
