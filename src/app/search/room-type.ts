export class RoomType {
    id: number;
    type: string;
    paxes: number;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
