import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { MyDatePickerModule } from 'mydatepicker';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchService } from './search.service';
import { SearchComponent } from './search.component';
import { Search } from './search';
import { ROOMTYPES } from './mock-room-types';

describe('SearchComponent', () => {
    let component: SearchComponent;
    let fixture: ComponentFixture<SearchComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                SearchComponent
            ],
            providers: [
                { provide: SearchService, useClass: MockSearchService }
            ],
            imports: [
                MyDatePickerModule,
                RouterTestingModule.withRoutes([])
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should print dates with correct date pipe', () => {
        const bolds = fixture.debugElement.query(By.css('.navbar-text')).nativeElement;
        expect(bolds.textContent).toContain('16 Apr 2017');
        expect(bolds.textContent).toContain('17 Apr 2017');
    });
});

class MockSearchService {
    getSearchFromLS(): Search {
        return new Search({
            hotel: 'Sant Francesc Hotel Singular',
            startDate: new Date('Sunday, 04/16/17'),
            endDate: new Date('Monday, 04/17/17'),
            roomType: {}
        });
    }
    getSearchInfo(): Search {
        return new Search();
    }
    getRoomTypes() {
        return Promise.resolve(ROOMTYPES);
    }
}
