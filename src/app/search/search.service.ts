import { Injectable } from '@angular/core';

import { Search } from './search';
import { RoomType } from './room-type';

import { ROOMTYPES } from './mock-room-types';

@Injectable()
export class SearchService {
    private searchInfo: Search = new Search();

    setSearchInfo(search: Search): void {
        this.searchInfo = search;
    }

    getSearchInfo(): Search {
        return this.searchInfo;
    }

    getRoomTypes() {
        return Promise.resolve(ROOMTYPES);
    }

    getSearchFromLS(): Search {
        let searchInfo = JSON.parse(localStorage.getItem('searchInfo'));
        if (!searchInfo) {
            searchInfo = JSON.parse('{"hotel":"Sant Francesc Hotel Singular",' +
                '"startDate":"Sunday, 04/16/17","endDate":"Monday, 04/17/17","roomType":"Double Room"}');
        }
        return new Search({
            hotel: searchInfo.hotel,
            startDate: new Date(searchInfo.startDate),
            endDate: new Date(searchInfo.endDate),
            roomType: this.filterRoomType(searchInfo.roomType)
        });
    }

    filterRoomType(type): RoomType {
        let filteredRoomTypes = ROOMTYPES.filter(
            roomType => roomType.type === type
        );

        if (filteredRoomTypes.length < 1) {
            filteredRoomTypes = ROOMTYPES.filter(
                roomType => roomType.type === 'Double Room'
            );
        }
        return filteredRoomTypes[0];
    }
}
