import { RoomType } from './room-type';

export let ROOMTYPES: RoomType[] = [
    {
        'id': 0,
        'type': 'Single Room',
        'paxes': 1
    },
    {
        'id': 1,
        'type': 'Double Room',
        'paxes': 2
    },
    {
        'id': 2,
        'type': 'Family Rooms',
        'paxes': 3
    },
    {
        'id': 3,
        'type': 'Multiple Rooms',
        'paxes': 4
    }
];
