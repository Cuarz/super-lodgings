import { RoomType } from './room-type';

export class Search {
    hotel: string;
    startDate: Date;
    endDate: Date;
    roomType: RoomType;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
