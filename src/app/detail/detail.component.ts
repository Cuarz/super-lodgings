import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IMyOptions } from 'mydatepicker';

import { User } from './user';
import { Booking } from './../booking/booking';
import { Payment } from './payment';

import { BookingService } from './../booking/booking.service';
import { ToastrService } from './../shared/toastr/toastr.service';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
    booking: Booking = new Booking();

    myDatePickerOptions: IMyOptions = {
        dateFormat: 'dd/mm/yyyy',
        showTodayBtn: false,
        firstDayOfWeek: 'mo',
        sunHighlight: false,
        maxYear: 2017,
        showClearDateBtn: false,
        selectionTxtFontSize: '14px',
        openSelectorTopOfInput: true,
        showSelectorArrow: false
    };

    constructor(
        private bookingService: BookingService,
        private router: Router,
        private toastr: ToastrService) {
    }

    ngOnInit() {
        if (!this.bookingService.getBooking().user) {
            this.booking.user = new User();
            this.booking.payment = new Payment();
        } else {
            this.booking = this.bookingService.getBooking();
        }

    }

    onNameChange(): void {
        this.booking.travelerName = this.booking.user.name;
    }

    onSurnameChange(): void {
        this.booking.travelerSurname = this.booking.user.surname;
    }

    onCardPress(): void {
        this.booking.payment.cardNumber = this.booking.payment.cardNumber.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
    }

    onContinue({ value, valid }): void {
        if (!valid) {
            this.toastr.showError('Please check the inputs with the red mark', 'Your details are not valid');
        } else {
            this.setBooking();
            this.router.navigate(['/booking']);
        }
    }

    onBack(): void {
        this.setBooking();
        this.router.navigate(['/search']);
    }

    setBooking(): void {
        this.bookingService.setBooking(this.booking);
    }

}
