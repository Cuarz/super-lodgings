import { ToastrService } from './../shared/toastr/toastr.service';
import { BookingService } from './../booking/booking.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MyDatePickerModule } from 'mydatepicker';
import { DetailComponent } from './detail.component';

describe('DetailComponent', () => {
    let component: DetailComponent;
    let fixture: ComponentFixture<DetailComponent>;
    const toastrServiceStub = {};

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                DetailComponent
            ],
            imports: [
                FormsModule,
                MyDatePickerModule,
                RouterTestingModule.withRoutes([]),
            ],
            providers: [
                BookingService,
                { provide: ToastrService, useValue: toastrServiceStub }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
