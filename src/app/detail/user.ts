export class User {
    name: string;
    surname: string;
    address: string;
    postalCode: number;
    location: string;
    country: string;
    birthDate: Date;
    phone: string;
    email: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
