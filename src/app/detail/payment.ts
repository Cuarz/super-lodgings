export class Payment {
    cardNumber: string;
    cardName: string;
    expDate: string;
    cvv: number;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
