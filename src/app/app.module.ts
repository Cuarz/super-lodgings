import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { BsDropdownModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { MyDatePickerModule } from 'mydatepicker';
import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { CustomOption } from './shared/toastr/custom-options';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HeaderComponent } from './header/header.component';
import { SearchComponent } from './search/search.component';
import { RoomListComponent } from './room/room-list/room-list.component';
import { RoomComponent } from './room/room/room.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { LanguageSelectorComponent } from './header/language-selector/language-selector.component';
import { DetailComponent } from './detail/detail.component';
import { BookingComponent } from './booking/booking.component';
import { DoneComponent } from './done/done.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { SearchService } from './search/search.service';
import { RoomService } from './room/room/room.service';
import { ShoppingCartService } from './shopping-cart/shopping-cart.service';
import { BookingService } from './booking/booking.service';
import { ToastrService } from './shared/toastr/toastr.service';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        BsDropdownModule.forRoot(),
        AccordionModule.forRoot(),
        ToastModule.forRoot(),
        MyDatePickerModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        SearchComponent,
        RoomListComponent,
        RoomComponent,
        PageNotFoundComponent,
        ShoppingCartComponent,
        LanguageSelectorComponent,
        DetailComponent,
        BookingComponent,
        DoneComponent
    ],
    providers: [
        SearchService,
        RoomService,
        ShoppingCartService,
        BookingService,
        ToastrService,
        { provide: ToastOptions, useClass: CustomOption }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
