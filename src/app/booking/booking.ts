import { Payment } from './../detail/payment';
import { Room } from './../room/room/room';
import { User } from './../detail/user';

export class Booking {
    hotelName: string;
    user: User;
    rooms: Room[];
    payment: Payment;
    travelerName: string;
    travelerSurname: string;
    message: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
