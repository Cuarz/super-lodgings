import { Injectable } from '@angular/core';
import { Booking } from './booking';

@Injectable()
export class BookingService {
    private booking: Booking = new Booking();

    setBooking(booking: Booking): void {
        this.booking = booking;
    }

    getBooking(): Booking {
        return this.booking;
    }

    save(booking: Booking) {
        // Post so save the completed booking, should check availability before booking!
        // mocked promise to emulate post
        return new Promise((resolve, reject) => {
            resolve(true);
        });
    }

}
