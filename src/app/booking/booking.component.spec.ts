import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Booking } from './booking';
import { ToastrService } from './../shared/toastr/toastr.service';
import { ShoppingCartService } from './../shopping-cart/shopping-cart.service';
import { BookingService } from './booking.service';
import { BookingComponent } from './booking.component';

const expectedBooking = new Booking({
    user: {
        name: 'Carlo',
        surname: 'Hartung',
        address: 'C/ falsa 123',
        postalCode: '07180',
        location: 'Santa Ponsa',
        country: 'Spain',
        birthDate: {
            formatted: '16/06/1992',
        },
        email: 'carlo@carlo.es',
        phone: '1111111'
    },
    payment: {
        cardNumber: '4444 4444 4444 4444',
        cardName: 'Carlo Hartung',
        cardExp: '10/20',
        cvv: '111'
    },
    travelerName: 'Carlo',
    travelerSurname: 'Hartung',
    rooms: [
        {
            id: 2,
            type: 'Doble',
            persons: 2,
            maxPersons: 2,
            price: 30,
            arrivalDate: '2017-04-15T22:00:00.000Z',
            departureDate: '2017-04-16T22:00:00.000Z',
            hotelName: 'Sant Francesc Hotel Singular'
        }
    ],
    hotelName: 'Sant Francesc Hotel Singular'
});

describe('BookingComponent', () => {
    let component: BookingComponent;
    let fixture: ComponentFixture<BookingComponent>;
    const toastrServiceStub = {};


    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                BookingComponent
            ],
            providers: [
                ShoppingCartService,
                { provide: BookingService, useClass: MockBookingService },
                { provide: ToastrService, useValue: toastrServiceStub }
            ],
            imports: [
                AccordionModule.forRoot(),
                FormsModule,
                RouterTestingModule.withRoutes([]),
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BookingComponent);
        component = fixture.componentInstance;
        component.booking = expectedBooking;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should print the user detail correctly', () => {
        const formFields = fixture.debugElement.queryAll(By.css('.form-group'));
        const cardField = fixture.debugElement.query(By.css('#cardLabel')).nativeElement;

        expect(formFields[0].nativeElement.textContent).toContain('Carlo');
        expect(formFields[1].nativeElement.textContent).toContain('Hartung');
        expect(formFields[2].nativeElement.textContent).toContain('C/ falsa 123');
        expect(formFields[3].nativeElement.textContent).toContain('07180');
        expect(formFields[4].nativeElement.textContent).toContain('Santa Ponsa');
        expect(formFields[5].nativeElement.textContent).toContain('Spain');
        expect(formFields[6].nativeElement.textContent).toContain('16/06/1992');
        expect(formFields[7].nativeElement.textContent).toContain('carlo@carlo.es');
        expect(formFields[8].nativeElement.textContent).toContain('1111111');
        expect(formFields[9].nativeElement.textContent).toContain('Carlo');
        expect(formFields[10].nativeElement.textContent).toContain('Hartung');

        expect(cardField.textContent).toContain('**** **** **** 4444');
    });
});

class MockBookingService {
    getBooking(): Booking {
        return expectedBooking;
    }
}
