import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { Booking } from './booking';
import { Room } from './../room/room/room';
import { BookingService } from './booking.service';
import { ShoppingCartService } from './../shopping-cart/shopping-cart.service';
import { ToastrService } from './../shared/toastr/toastr.service';

@Component({
    selector: 'app-booking',
    templateUrl: './booking.component.html',
    styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {
    private shoppingCartItems$: Observable<Room[]> = of([]);
    private totalSellPrice$: Observable<number> = of();
    totalSellPrice = 0;
    booking: Booking = new Booking();
    cardLastNumbers = '';

    constructor(
        private router: Router,
        private bookingService: BookingService,
        private cartService: ShoppingCartService,
        private toastr: ToastrService) {
            this.shoppingCartItems$ = this.cartService.getItems();
            this.shoppingCartItems$.subscribe(rooms => this.booking.rooms = rooms);

            this.totalSellPrice$ = this.cartService.getTotalSellPrice();
            this.totalSellPrice$.subscribe(total => this.totalSellPrice = total);
    }

    ngOnInit() {
        this.buildBooking();
    }

    buildBooking(): void {
        const tmpRooms: Room[] = this.booking.rooms;
        this.booking = this.bookingService.getBooking();
        this.booking.rooms = tmpRooms;
        this.booking.hotelName = this.booking.rooms[0].hotelName;
        this.cardLastNumbers = this.booking.payment.cardNumber.substr(this.booking.payment.cardNumber.length - 4);
    }

    deleteRoom(room: Room): void {
        this.cartService.removeFromCart(room);
        this.toastr.showInfo('Room removed from your booking and basket');
    }

    saveBooking(): void {
        if (this.booking.rooms.length > 0) {
            // mocked promise for booking save
            this.bookingService.save(this.booking).then((value) => {
                this.cartService.discardCart();
                this.router.navigate(['/done']);
            }, (error) => {
                this.toastr.showError('Error: ' + error, 'Error on booking completion!')
            })
        } else {
            this.toastr.showWarning('Please, add some rooms before you book', 'You have no more rooms!');
        }
    }

}
