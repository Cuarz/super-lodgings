import { Injectable } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Injectable()
export class ToastrService {

    constructor(
        private toastr: ToastsManager) {
     }

    showSuccess(message: string, title?: string) {
        this.toastr.success(message, title);
    }

    showError(message: string, title?: string) {
        this.toastr.error(message, title);
    }

    showWarning(message: string, title?: string) {
        this.toastr.warning(message, title);
    }

    showInfo(message: string, title?: string) {
        this.toastr.info(message, title);
    }

}
