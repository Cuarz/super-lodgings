import { TestBed, inject } from '@angular/core/testing';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { ToastrService } from './toastr.service';

describe('ToastrService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ToastrService],
            imports: [ToastModule.forRoot()]
        });
    });

    it('should ...', inject([ToastrService], (service: ToastrService) => {
        expect(service).toBeTruthy();
    }));
});
