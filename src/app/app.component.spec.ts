import { ToastrService } from './shared/toastr/toastr.service';
import { ShoppingCartService } from './shopping-cart/shopping-cart.service';
import { LanguageSelectorComponent } from './header/language-selector/language-selector.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { HeaderComponent } from './header/header.component';
import { TestBed, async } from '@angular/core/testing';
import { ToastsManager, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { RouterTestingModule } from '@angular/router/testing';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
    const toastrServiceStub = {};

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                HeaderComponent,
                ShoppingCartComponent,
                LanguageSelectorComponent
            ],
            providers: [
                ShoppingCartService,
                { provide: ToastrService, useValue: toastrServiceStub },
                ToastsManager,
                ToastOptions
            ],
            imports: [
                RouterTestingModule.withRoutes([]),
                BsDropdownModule.forRoot()
            ]
        })
        .compileComponents();
    }));

    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
});
