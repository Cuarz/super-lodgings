import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { of } from 'rxjs/observable/of';

import { Room } from './../room/room/room';

import { ShoppingCartService } from './shopping-cart.service';
import { ToastrService } from './../shared/toastr/toastr.service';

@Component({
    selector: 'app-shopping-cart',
    templateUrl: './shopping-cart.component.html',
    styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
    private shoppingCartItems$: Observable<Room[]> = of([]);
    public shoppingCartItems: Room[] = [];

    private totalSellPrice$: Observable<number> = of();
    public totalSellPrice = 0;


    constructor(
        private cartService: ShoppingCartService,
        private router: Router,
        private toastr: ToastrService) {
            this.shoppingCartItems$ = this.cartService.getItems();
            this.shoppingCartItems$.subscribe(rooms => this.shoppingCartItems = rooms);

            this.totalSellPrice$ = this.cartService.getTotalSellPrice();
            this.totalSellPrice$.subscribe(total => this.totalSellPrice = total);
    }

    ngOnInit() {
        this.setCartFromSession();
    }

    setCartFromSession() {
        this.cartService.setCartFromSession();
    }

    onBook(): void {
        if (this.shoppingCartItems.length > 0) {
            this.router.navigate(['/detail']);
        } else {
            this.toastr.showWarning('Please, add some rooms before you continue', 'Basket is empty!');
        }
    }

    deleteRoom(room: Room): void {
        this.cartService.removeFromCart(room);
        this.toastr.showInfo('Room removed from your basket');
    }

    discardCart(): void {
        this.cartService.discardCart();
        this.toastr.showInfo('Have fun adding new rooms!', 'Your basket is empty now');
    }

}
