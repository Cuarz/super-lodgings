import { ToastrService } from './../shared/toastr/toastr.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ShoppingCartComponent } from './shopping-cart.component';
import { ShoppingCartService } from './shopping-cart.service';
import { Room } from './../room/room/room';

describe('ShoppingCartComponent', () => {
    let component: ShoppingCartComponent;
    let fixture: ComponentFixture<ShoppingCartComponent>;
    const toastrServiceStub = {};

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ShoppingCartComponent
            ],
            providers: [
                ShoppingCartService,
                { provide: ToastrService, useValue: toastrServiceStub }
            ],
            imports: [
                BsDropdownModule.forRoot(),
                RouterTestingModule.withRoutes([])
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ShoppingCartComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should print 0 on counter', () => {
        const counter = fixture.debugElement.query(By.css('#itemsLength')).nativeElement;
        expect(counter.textContent).toEqual(' 0 ');
    });

    it('should have a total of 0', () => {
        expect(component.totalSellPrice).toEqual(0);
    });

    it('should update counter on adding a room', () => {
        let counter;
        component.shoppingCartItems.push(new Room({
            id: 2,
            type: 'Doble',
            persons: 2,
            maxPersons: 2,
            price: 30,
            arrivalDate: '2017-04-15T22:00:00.000Z',
            departureDate: '2017-04-16T22:00:00.000Z',
            hotelName: 'Sant Francesc Hotel Singular'
        }));
        fixture.detectChanges();
        counter = fixture.debugElement.query(By.css('#itemsLength')).nativeElement;

        expect(counter.textContent).toEqual(' 1 ');
    });
});
