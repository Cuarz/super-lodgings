import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, Subject, Subscriber } from 'rxjs';
import { of } from 'rxjs/observable/of';

import { Room } from './../room/room/room';

@Injectable()
export class ShoppingCartService {
    private itemsInCartSubject: BehaviorSubject<Room[]> = new BehaviorSubject([]);
    private itemsInCart: Room[] = [];

    constructor() {
        this.itemsInCartSubject.subscribe(rooms => this.itemsInCart = rooms);
    }

    getItems(): Observable<Room[]> {
        return this.itemsInCartSubject;
    }

    getTotalSellPrice(): Observable<number> {
        return this.itemsInCartSubject.map((items: Room[]) => {
            return items.reduce((prev, curr: Room) => {
                return prev + curr.price;
            }, 0);
        });
    }

    setCartFromSession(): void {
        if (sessionStorage.getItem('myCart')) {
            this.itemsInCartSubject.next(JSON.parse(sessionStorage.getItem('myCart')));
        }
    }

    addToCart(item: Room): boolean {
        const currentItems = [...this.itemsInCart];
        const sameRoomAndDates = currentItems.find(
            room => room.id === item.id
                    && (room.arrivalDate <= item.departureDate
                    && room.departureDate >= item.arrivalDate));
        if (!sameRoomAndDates) {
            this.itemsInCartSubject.next([...this.itemsInCart, item]);
            sessionStorage.setItem('myCart', JSON.stringify(this.itemsInCart));
            return true;
        }
        return false;
    }

    removeFromCart(item: Room): void {
        const currentItems = [...this.itemsInCart];
        const index: number = currentItems.indexOf(item);
        if (index !== -1) {
            currentItems.splice(index, 1);
        }
        this.itemsInCartSubject.next(currentItems);

        if (this.itemsInCart.length > 0) {
            sessionStorage.setItem('myCart', JSON.stringify(this.itemsInCart));
        } else {
            sessionStorage.removeItem('myCart');
        }
    }

    discardCart(): void {
        this.itemsInCartSubject.next([]);
        sessionStorage.removeItem('myCart');
    }

}
