import { TestBed, inject } from '@angular/core/testing';
import { Room } from './../room/room/room';
import { ShoppingCartService } from './shopping-cart.service';

describe('ShoppingCartService', () => {
    const expectedRoom = new Room({
        id: 2,
        type: 'Doble',
        persons: 2,
        maxPersons: 2,
        price: 30,
        arrivalDate: '2017-04-15T22:00:00.000Z',
        departureDate: '2017-04-16T22:00:00.000Z',
        hotelName: 'Sant Francesc Hotel Singular'
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ShoppingCartService]
        });
    });

    it('should create service', inject([ShoppingCartService], (service: ShoppingCartService) => {
        expect(service).toBeTruthy();
    }));

    it('should add and remove room', inject([ShoppingCartService], (service: ShoppingCartService) => {
        let counter = 0;
        service.addToCart(expectedRoom);

        service.getItems().subscribe((room) => {
            if (counter === 0) {
                expect(room.length).toBe(1);
            } else {
                expect(room.length).toBe(0);
            }
            counter += 1;
        });

        service.removeFromCart(expectedRoom);
    }));
});
