import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { of } from 'rxjs/observable/of';

import { Room } from './../room/room';
import { Search } from './../../search/search';
import { RoomService } from './../room/room.service';
import { SearchService } from './../../search/search.service';
import { ShoppingCartService } from './../../shopping-cart/shopping-cart.service';
import { ToastrService } from './../../shared/toastr/toastr.service';

@Component({
    selector: 'app-room-list',
    templateUrl: './room-list.component.html',
    styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {
    private shoppingCartItems$: Observable<Room[]> = of([]);
    public shoppingCartItems: Room[] = [];
    public rooms: Room[] = [];
    public search: Search = new Search();

    constructor(
        private roomService: RoomService,
        private searchService: SearchService,
        private cartService: ShoppingCartService,
        private toastr: ToastrService,
        private router: Router) {
            this.shoppingCartItems$ = this.cartService.getItems();
            this.shoppingCartItems$.subscribe(rooms => this.shoppingCartItems = rooms);
    }

    ngOnInit() {
        this.search = this.searchService.getSearchInfo();
        this.getRooms();
        if (this.areDatesEmpty()) {
            this.router.navigate(['/search']);
        }
    }

    private getRooms() {
        this.roomService.getRooms(this.search).then(
            rooms => this.rooms = rooms
        );
    }

    private areDatesEmpty(): boolean {
        if (!this.search.endDate || !this.search.startDate) {
            return true;
        }
        return false;
    }

    deleteRoomFromList(room: Room): void {
        const currentItems = [...this.rooms];
        const itemsWithoutRemoved = currentItems.filter(_ => _.id !== room.id);
        this.rooms = itemsWithoutRemoved;
    }

    onBook(): void {
        if (this.shoppingCartItems.length > 0) {
            this.router.navigate(['/detail']);
        } else {
            this.toastr.showWarning('Please, add some rooms before you continue', 'Basket is empty!');
        }
    }

}
