import { RoomComponent } from './../room/room.component';
import { SearchService } from './../../search/search.service';
import { RoomService } from './../room/room.service';
import { ShoppingCartService } from './../../shopping-cart/shopping-cart.service';
import { ToastrService } from './../../shared/toastr/toastr.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RoomListComponent } from './room-list.component';

describe('RoomListComponent', () => {
    let component: RoomListComponent;
    let fixture: ComponentFixture<RoomListComponent>;
    const toastrServiceStub = {};

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                RoomListComponent,
                RoomComponent
            ],
            providers: [
                ShoppingCartService,
                SearchService,
                RoomService,
                { provide: ToastrService, useValue: toastrServiceStub }
            ],
            imports: [
                RouterTestingModule.withRoutes([])
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RoomListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
