import { Injectable } from '@angular/core';
import { ROOMS } from './mock-rooms';
import { Search } from './../../search/search';


@Injectable()
export class RoomService {
    getRooms(search: Search) {
        // mock filter, this should be done on api side, the same for the date availability
        if (search.roomType) {
            const filteredRooms = ROOMS.filter(
                room => room.persons === search.roomType.paxes
            );
            return Promise.resolve(filteredRooms);
        }
        return Promise.resolve(ROOMS);
    }
}
