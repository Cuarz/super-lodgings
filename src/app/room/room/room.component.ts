import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Room } from './room';
import { Search } from './../../search/search';

import { ShoppingCartService } from './../../shopping-cart/shopping-cart.service';
import { ToastrService } from './../../shared/toastr/toastr.service';


@Component({
    selector: 'app-room',
    templateUrl: './room.component.html',
    styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {
    @Input() room: Room;
    @Input() search: Search;
    @Output() onRoomSelect: EventEmitter <Room> = new EventEmitter();

    constructor(private cartService: ShoppingCartService,
                private toastr: ToastrService) {
    }
    ngOnInit() {
    }

    addRoomToCart(room: Room): void {
        const selectedRoom = {...room};
        let addedToCart = false;
        selectedRoom.arrivalDate = this.search.startDate;
        selectedRoom.departureDate = this.search.endDate;
        selectedRoom.hotelName = this.search.hotel;
        addedToCart = this.cartService.addToCart(selectedRoom);

        if (addedToCart) {
            this.toastr.showSuccess('Room added to your basket', 'Great!');
            this.onRoomSelect.emit(selectedRoom);
        } else {
            this.toastr.showWarning('Impossible to add the same room for the same dates!', 'Try another room or dates');
        }
    }

}
