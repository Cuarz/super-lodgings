import { Room } from './room';

export let ROOMS: Room[] = [
    {
        'id': 0,
        'type': 'Individual',
        'persons': 1,
        'maxPersons': 1,
        'price': 45.00,
        'arrivalDate': null,
        'departureDate': null,
        'hotelName': null
    },
    {
        'id': 1,
        'type': 'Individual',
        'persons': 1,
        'maxPersons': 1,
        'price': 50.00,
        'arrivalDate': null,
        'departureDate': null,
        'hotelName': null
    },
    {
        'id': 2,
        'type': 'Doble',
        'persons': 2,
        'maxPersons': 2,
        'price': 30.00,
        'arrivalDate': null,
        'departureDate': null,
        'hotelName': null
    },
    {
        'id': 3,
        'type': 'Doble',
        'persons': 2,
        'maxPersons': 2,
        'price': 50.00,
        'arrivalDate': null,
        'departureDate': null,
        'hotelName': null
    },
    {
        'id': 4,
        'type': 'Doble',
        'persons': 2,
        'maxPersons': 2,
        'price': 50.00,
        'arrivalDate': null,
        'departureDate': null,
        'hotelName': null
    },
    {
        'id': 5,
        'type': 'Doble',
        'persons': 2,
        'maxPersons': 2,
        'price': 100.00,
        'arrivalDate': null,
        'departureDate': null,
        'hotelName': null
    },
    {
        'id': 6,
        'type': 'Family',
        'persons': 3,
        'maxPersons': 3,
        'price': 150.00,
        'arrivalDate': null,
        'departureDate': null,
        'hotelName': null
    },
    {
        'id': 7,
        'type': 'Multiple',
        'persons': 4,
        'maxPersons': 4,
        'price': 200.00,
        'arrivalDate': null,
        'departureDate': null,
        'hotelName': null
    }
];
