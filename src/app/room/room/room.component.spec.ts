import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ShoppingCartService } from './../../shopping-cart/shopping-cart.service';
import { ToastrService } from './../../shared/toastr/toastr.service';
import { Room } from './room';
import { RoomComponent } from './room.component';

describe('RoomComponent', () => {
    let component: RoomComponent;
    let fixture: ComponentFixture<RoomComponent>;
    const toastrServiceStub = {};
    let expectedRoom: Room;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                RoomComponent
            ],
            providers: [
                ShoppingCartService,
                { provide: ToastrService, useValue: toastrServiceStub }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RoomComponent);
        component = fixture.componentInstance;

        expectedRoom = new Room({
            id: 0,
            type: 'Individual',
            persons: 1,
            maxPersons: 1,
            price: 45.00,
            arrivalDate: null,
            departureDate: null,
            hotelName: null
        });
        component.room = expectedRoom;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should print price correctly with the custom pipe', () => {
        const roomFields = fixture.debugElement.queryAll(By.css('.bold'));
        expect(roomFields[2].nativeElement.textContent).toContain('45.00');
    });
});
