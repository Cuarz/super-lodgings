export class Room {
    id: number;
    type: string;
    persons: number;
    maxPersons: number;
    price: number;
    arrivalDate: Date;
    departureDate: Date;
    hotelName: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
