import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    search = '/search';
    roomList = '/room-list';
    detail = '/detail';
    booking = '/booking';
    done = '/done';

    constructor(
        public router: Router) {
    }

    ngOnInit() {
    }

}
