import { ToastrService } from './../shared/toastr/toastr.service';
import { ShoppingCartService } from './../shopping-cart/shopping-cart.service';
import { LanguageSelectorComponent } from './language-selector/language-selector.component';
import { ShoppingCartComponent } from './../shopping-cart/shopping-cart.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BsDropdownModule } from 'ngx-bootstrap';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;
    const toastrServiceStub = {};

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                HeaderComponent,
                LanguageSelectorComponent,
                ShoppingCartComponent
            ],
            providers: [
                ShoppingCartService,
                { provide: ToastrService, useValue: toastrServiceStub }
            ],
            imports: [
                BsDropdownModule.forRoot(),
                RouterTestingModule.withRoutes([])
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
